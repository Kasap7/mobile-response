package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class TFPollActivity extends AppCompatActivity {

    private EditText etTFPollTitle;
    private Button btnBack;
    private Button btnCreatePoll;

    private long poll_counter = -1;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabasePollCounter;

    private boolean liveView;
    private boolean privateView;
    private boolean multipleAnswers;
    private int unlockedPosition = 0;
    private int lockedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tfpoll);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(TFPollActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Polls");
        mDatabasePollCounter = FirebaseDatabase.getInstance().getReference().child("poll_counter");

        mDatabasePollCounter.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                poll_counter = (Long)dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        etTFPollTitle = (EditText) findViewById(R.id.etTFPollTitle);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnCreatePoll = (Button) findViewById(R.id.btnCreatePoll);

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            liveView = bundle.getBoolean("live_view");
            privateView = bundle.getBoolean("private_view");
            multipleAnswers = bundle.getBoolean("multiple_answers");
            unlockedPosition = bundle.getInt("unlocked_position");
            lockedPosition = bundle.getInt("locked_position");
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TFPollActivity.this, NewPollActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                intent.putExtra("live_view", liveView);
                intent.putExtra("private_view", privateView);
                intent.putExtra("multiple_answers", multipleAnswers);
                intent.putExtra("unlocked_position", unlockedPosition);
                intent.putExtra("locked_position", lockedPosition);
                intent.putExtra("poll_type_position", 0);

                startActivity(intent);
            }
        });

        btnCreatePoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = etTFPollTitle.getText().toString().trim();

                if (TextUtils.isEmpty(title)) {
                    Toast.makeText(TFPollActivity.this, "Title must be provided.", Toast.LENGTH_LONG).show();
                    return;
                }

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("title", title);

                Map<String, Object> answers = new HashMap<>();
                answers.put("true" , 0);
                answers.put("false" , 0);
                childUpdates.put("answers", answers);

                long timeNow = System.currentTimeMillis();
                long unlock = timeNow + Utility.UNLOCK_MAP.get(unlockedPosition);
                long lock = timeNow + Utility.LOCK_MAP.get(lockedPosition);

                childUpdates.put("unlock", unlock);
                childUpdates.put("lock", lock);
                childUpdates.put("live_results", liveView);
                childUpdates.put("private_results", privateView);
                childUpdates.put("multiple_vote", multipleAnswers);
                childUpdates.put("creator", mAuth.getCurrentUser().getUid());
                childUpdates.put("answered", 0L);

                final String pollName = "tf" + poll_counter;
                mDatabase.child(pollName).updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mDatabasePollCounter.setValue(poll_counter + 1L);

                        Intent mainIntent = new Intent(TFPollActivity.this, MainActivity.class);
                        mainIntent.putExtra("new_poll", pollName);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_audience, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_vote_poll) {
            Intent lecturerIntent = new Intent(this, SearchPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
