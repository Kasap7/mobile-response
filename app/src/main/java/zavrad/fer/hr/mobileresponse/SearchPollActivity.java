package zavrad.fer.hr.mobileresponse;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SearchPollActivity extends AppCompatActivity {

    private Spinner spPollOptions;
    private EditText etSearchPoll;
    private Button btnSubmit;
    private int selectedItemPosition = 0;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_poll);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(SearchPollActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                    return;
                } else {
                }
            }
        };

        etSearchPoll = (EditText) findViewById(R.id.etSearchPoll);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        spPollOptions = (Spinner) findViewById(R.id.spPollOptions);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.poll_search_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPollOptions.setAdapter(adapter);

        spPollOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItemPosition = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String pollName = etSearchPoll.getText().toString();
                FirebaseDatabase.getInstance().getReference().child("Polls").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChild(pollName)) {
                            Toast.makeText(SearchPollActivity.this,
                                    "Given poll id does not match any existing poll.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (pollName.startsWith("mo")) {
                            if (selectedItemPosition == 0) {
                                Intent intent = new Intent(SearchPollActivity.this, MOPollVoteActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            } else if (selectedItemPosition == 1) {
                                Intent intent = new Intent(SearchPollActivity.this, MOPollResultActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            }
                        } else if (pollName.startsWith("mm")) {
                            if (selectedItemPosition == 0) {
                                Intent intent = new Intent(SearchPollActivity.this, MMPollVoteActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            } else if (selectedItemPosition == 1) {
                                Intent intent = new Intent(SearchPollActivity.this, MMPollResultActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            }
                        } else if (pollName.startsWith("tf")) {
                            if (selectedItemPosition == 0) {
                                Intent intent = new Intent(SearchPollActivity.this, TFPollVoteActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            } else if (selectedItemPosition == 1) {
                                Intent intent = new Intent(SearchPollActivity.this, TFPollResultActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            }
                        } else if (pollName.startsWith("tt")) {
                            if (selectedItemPosition == 0) {
                                Intent intent = new Intent(SearchPollActivity.this, TTPollVoteActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            } else if (selectedItemPosition == 1) {
                                Intent intent = new Intent(SearchPollActivity.this, TTPollResultActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            }
                        } else if (pollName.startsWith("tn")) {
                            if (selectedItemPosition == 0) {
                                Intent intent = new Intent(SearchPollActivity.this, TTPollVoteActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            } else if (selectedItemPosition == 1) {
                                Intent intent = new Intent(SearchPollActivity.this, TTPollResultActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("poll_name", pollName);
                                startActivity(intent);
                                return;
                            }
                        } else {
                            Toast.makeText(SearchPollActivity.this, "Internal error.", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("error") != null) {
            Bundle bundle = getIntent().getExtras();
            String errorMessage = bundle.getString("error");
            ErrorMessageFragment fragment = new ErrorMessageFragment();
            fragment.errorMessage(errorMessage);
            fragment.show(getFragmentManager(), "error_message");
        }
    }

    public static class ErrorMessageFragment extends DialogFragment {
        private String errorMessage = "";
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(errorMessage)
                    .setPositiveButton(R.string.poll_created_positive, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
            return builder.create();
        }

        private void errorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_lecturer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_create_poll) {
            Intent lecturerIntent = new Intent(this, NewPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
