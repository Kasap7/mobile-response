package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TFPollVoteActivity extends AppCompatActivity {

    private static final String TAG = "TFPollVoteActivity_TAG";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String pollName;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabasePollVotes;

    private TextView tvTitle;
    private Button btnSubmit;
    private RadioButton rbTrue;
    private RadioButton rbFalse;

    private boolean liveVote;
    private boolean privateView;
    private boolean multipleAnswers;
    private String creator;
    private long unlock;
    private long lock;
    private long answered;

    private boolean voted = false;

    private View.OnClickListener rbListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            rbTrue.setChecked(false);
            rbFalse.setChecked(false);

            if (id == R.id.rbTrue) {
                rbTrue.setChecked(true);
            } else if (id == R.id.rbFalse) {
                rbFalse.setChecked(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tfpoll_vote);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(TFPollVoteActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Polls");
        mDatabasePollVotes = FirebaseDatabase.getInstance().getReference().child("Poll_votes");

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        rbTrue = (RadioButton) findViewById(R.id.rbTrue);
        rbFalse = (RadioButton) findViewById(R.id.rbFalse);

        rbTrue.setChecked(true);

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("poll_name") != null) {
            Bundle bundle = getIntent().getExtras();
            pollName = bundle.getString("poll_name");

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (voted) {
                        Log.d(TAG, "Init database listener after voting, error!");
                        return;
                    }
                    if (!dataSnapshot.hasChild(pollName)) {
                        Intent intent = new Intent(TFPollVoteActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Given POLL ID does not match any existing poll.");
                        startActivity(intent);
                        return;
                    }

                    unlock = (Long)dataSnapshot.child(pollName).child("unlock").getValue();
                    lock = (Long)dataSnapshot.child(pollName).child("lock").getValue();
                    privateView = (Boolean)dataSnapshot.child(pollName).child("private_results").getValue();
                    liveVote = (Boolean)dataSnapshot.child(pollName).child("live_results").getValue();
                    multipleAnswers = (Boolean)dataSnapshot.child(pollName).child("multiple_vote").getValue();
                    creator = (String)dataSnapshot.child(pollName).child("creator").getValue();
                    answered = (Long)dataSnapshot.child(pollName).child("answered").getValue();

                    long timeNow = System.currentTimeMillis();

                    if (unlock > timeNow) {
                        Intent intent = new Intent(TFPollVoteActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        String time = new SimpleDateFormat("HH:mm:ss").format(new Date(unlock - timeNow));
                        intent.putExtra("error", "Poll is still not available, poll will be available in: " + time + ".");
                        startActivity(intent);
                        return;
                    }

                    if (lock < timeNow) {
                        Intent intent = new Intent(TFPollVoteActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll is already finished.");
                        startActivity(intent);
                        return;
                    }

                    final String uid = mAuth.getCurrentUser().getUid();
                    if (!multipleAnswers) {
                        mDatabasePollVotes.child(pollName).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Log.d(TAG, "1) Poll_votes value event.");
                                if (voted) {
                                    Log.d(TAG, "2) Triggered after voting.");
                                    return;
                                }
                                if (dataSnapshot.hasChild(uid)) {
                                    Log.d(TAG, "3) Has child uid: " + uid);
                                    Intent intent = new Intent(TFPollVoteActivity.this, SearchPollActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("error", "User has already voted on this poll.");
                                    startActivity(intent);
                                    return;
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                    tvTitle.setText((String)dataSnapshot.child(pollName).child("title").getValue());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            Intent intent = new Intent(TFPollVoteActivity.this, SearchPollActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("error", "Internal error, no bundle provided.");
            startActivity(intent);
            return;
        }

        rbTrue.setOnClickListener(rbListener);
        rbFalse.setOnClickListener(rbListener);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean trueChecked = rbTrue.isChecked();
                final String uid = mAuth.getCurrentUser().getUid();

                mDatabase.child(pollName).child("answers").addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, ".1] Polls value event.");
                        if (voted){
                            Log.d(TAG, ".2] Triggered after voting.");
                            return;
                        }
                        voted = true;

                        if (trueChecked) {
                            long votes = (Long)dataSnapshot.child("true").getValue();
                            mDatabase.child(pollName).child("answers").child("true").setValue(votes + 1L);
                        } else {
                            long votes = (Long)dataSnapshot.child("false").getValue();
                            mDatabase.child(pollName).child("answers").child("false").setValue(votes + 1L);
                        }

                        mDatabase.child(pollName).child("answered").setValue(answered + 1L);
                        mDatabasePollVotes.child(pollName).child(uid).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(TFPollVoteActivity.this, TFPollResultActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("poll_name", pollName);
                                    startActivity(intent);
                                    return;
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_lecturer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_create_poll) {
            Intent lecturerIntent = new Intent(this, NewPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
