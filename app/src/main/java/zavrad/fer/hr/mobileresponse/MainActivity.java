package zavrad.fer.hr.mobileresponse;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "MainActivity_TAG";
    private Button btnAudience;
    private Button btnLecturer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAudience = (Button)findViewById(R.id.btnAudience);
        btnLecturer = (Button)findViewById(R.id.btnLecturer);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                    Log.d(TAG, user.toString());
                }
            }
        };

        btnAudience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent audienceIntent = new Intent(MainActivity.this, SearchPollActivity.class);
                audienceIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(audienceIntent);
            }
        });

        btnLecturer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent lecturerIntent = new Intent(MainActivity.this, NewPollActivity.class);
                lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(lecturerIntent);
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("new_poll") != null) {
            Bundle bundle = getIntent().getExtras();

            String pollName = bundle.getString("new_poll");

            PollCreatedFragment fragment = new PollCreatedFragment();
            fragment.setPollName(pollName);
            fragment.show(getFragmentManager(), "poll_created");
        }
    }

    public static class PollCreatedFragment extends DialogFragment {
        private String pollName = "";
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You have created new poll:\n" + pollName)
                    .setPositiveButton(R.string.poll_created_positive, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
            return builder.create();
        }

        private void setPollName(String pollName) {
            this.pollName = pollName;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
