package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class MOPollResultActivity extends AppCompatActivity {

    private static final String TAG = "MOPollResultActivity_TAG";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String pollName;
    private DatabaseReference mDatabase;

    private BarChart bcResult;
    private TextView tvTitle;
    private TextView tvStatus;
    private TextView tvResponses;
    private Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mopoll_result);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(MOPollResultActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Polls");

        bcResult = (BarChart) findViewById(R.id.bcResult);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvResponses = (TextView) findViewById(R.id.tvResponses);
        btnExit = (Button) findViewById(R.id.btnExit);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MOPollResultActivity.this, SearchPollActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("poll_name") != null) {
            Bundle bundle = getIntent().getExtras();
            pollName = bundle.getString("poll_name");
            mDatabase.child(pollName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long lock = (Long)dataSnapshot.child("lock").getValue();
                    long unlock = (Long)dataSnapshot.child("unlock").getValue();
                    String creator = (String)dataSnapshot.child("creator").getValue();
                    boolean liveView = (Boolean)dataSnapshot.child("live_results").getValue();
                    boolean privateView = (Boolean)dataSnapshot.child("private_results").getValue();

                    long timeNow = System.currentTimeMillis();
                    String uid = mAuth.getCurrentUser().getUid();
                    boolean finished = timeNow >= lock;

                    if (timeNow < unlock) {
                        Intent intent = new Intent(MOPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll did not start yet, so results cannot be obtained.");
                        startActivity(intent);
                        return;
                    }

                    if (privateView && !uid.equals(creator)) {
                        Intent intent = new Intent(MOPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll results of this poll can only be seen by its creator.");
                        startActivity(intent);
                        return;
                    }

                    if (!liveView && !finished) {
                        Intent intent = new Intent(MOPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll results of this poll can only be seen after this poll is finished.");
                        startActivity(intent);
                        return;
                    }

                    String title = (String)dataSnapshot.child("title").getValue();
                    long answered = (Long)dataSnapshot.child("answered").getValue();
                    Map<String, Long> answerMap = new LinkedHashMap<>();
                    DataSnapshot answersSnapshot = dataSnapshot.child("answers");

                    answerMap.put(
                            (String)answersSnapshot.child("A").child("text").getValue(),
                            (Long)answersSnapshot.child("A").child("votes").getValue()
                    );
                    answerMap.put(
                            (String)answersSnapshot.child("B").child("text").getValue(),
                            (Long)answersSnapshot.child("B").child("votes").getValue()
                    );
                    if (answersSnapshot.hasChild("E")) {
                        answerMap.put(
                                (String)answersSnapshot.child("C").child("text").getValue(),
                                (Long)answersSnapshot.child("C").child("votes").getValue()
                        );
                        answerMap.put(
                                (String)answersSnapshot.child("D").child("text").getValue(),
                                (Long)answersSnapshot.child("D").child("votes").getValue()
                        );
                        answerMap.put(
                                (String)answersSnapshot.child("E").child("text").getValue(),
                                (Long)answersSnapshot.child("E").child("votes").getValue()
                        );
                    } else if (answersSnapshot.hasChild("D")) {
                        answerMap.put(
                                (String)answersSnapshot.child("C").child("text").getValue(),
                                (Long)answersSnapshot.child("C").child("votes").getValue()
                        );
                        answerMap.put(
                                (String)answersSnapshot.child("D").child("text").getValue(),
                                (Long)answersSnapshot.child("D").child("votes").getValue()
                        );
                    } else if (answersSnapshot.hasChild("C")) {
                        answerMap.put(
                                (String)answersSnapshot.child("C").child("text").getValue(),
                                (Long)answersSnapshot.child("C").child("votes").getValue()
                        );
                    }

                    tvTitle.setText(title);
                    if (!finished) {
                        tvStatus.setText("Poll is still active");
                    }
                    tvResponses.setText(answered + " responses");

                    ArrayList<BarEntry> barEntries = new ArrayList<>();
                    ArrayList<String> xData = new ArrayList<>();
                    int i = 0;
                    for (Map.Entry<String, Long> entry : answerMap.entrySet()) {
                        float percent = (entry.getValue()*100) / (float) answered;
                        barEntries.add(new BarEntry(percent, i++));
                        xData.add(entry.getKey());
                    }

                    BarDataSet barDataSet = new BarDataSet(barEntries, "Percent");
                    BarData barData = new BarData(xData, barDataSet);
                    bcResult.setData(barData);

                    XAxis xAxis = bcResult.getXAxis();
//                    xAxis.setLabelRotationAngle(-90);
                    xAxis.setDrawGridLines(false);
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    bcResult.getAxisLeft().setValueFormatter(new PercentFormatter());

                    bcResult.getAxisRight().setEnabled(false);

                    bcResult.setDescription("");
                    bcResult.getLegend().setEnabled(false);

                    bcResult.setClickable(false);
                    xAxis.setLabelsToSkip(0);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_lecturer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_create_poll) {
            Intent lecturerIntent = new Intent(this, NewPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
