package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TFPollResultActivity extends AppCompatActivity {

    private static final String TAG = "TFPollResultActivity_TAG";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String pollName;
    private DatabaseReference mDatabase;

    private PieChart pcResult;
    private TextView tvTitle;
    private TextView tvStatus;
    private TextView tvResponses;
    private Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tfpoll_result);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(TFPollResultActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Polls");

        pcResult = (PieChart) findViewById(R.id.pcResult);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvResponses = (TextView) findViewById(R.id.tvResponses);
        btnExit = (Button) findViewById(R.id.btnExit);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TFPollResultActivity.this, SearchPollActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("poll_name") != null) {
            Bundle bundle = getIntent().getExtras();
            pollName = bundle.getString("poll_name");

            mDatabase.child(pollName).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long lock = (Long)dataSnapshot.child("lock").getValue();
                    long unlock = (Long)dataSnapshot.child("unlock").getValue();
                    String creator = (String)dataSnapshot.child("creator").getValue();
                    boolean liveView = (Boolean)dataSnapshot.child("live_results").getValue();
                    boolean privateView = (Boolean)dataSnapshot.child("private_results").getValue();

                    long timeNow = System.currentTimeMillis();
                    String uid = mAuth.getCurrentUser().getUid();
                    boolean finished = timeNow >= lock;

                    if (timeNow < unlock) {
                        Intent intent = new Intent(TFPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll did not start yet, so results cannot be obtained.");
                        startActivity(intent);
                        return;
                    }

                    if (privateView && !uid.equals(creator)) {
                        Intent intent = new Intent(TFPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll results of this poll can only be seen by its creator.");
                        startActivity(intent);
                        return;
                    }

                    if (!liveView && !finished) {
                        Intent intent = new Intent(TFPollResultActivity.this, SearchPollActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("error", "Poll results of this poll can only be seen after this poll is finished.");
                        startActivity(intent);
                        return;
                    }

                    String title = (String)dataSnapshot.child("title").getValue();
                    long answered = (Long)dataSnapshot.child("answered").getValue();
                    long trueVotes = (Long)dataSnapshot.child("answers").child("true").getValue();
                    long falseVotes = (Long)dataSnapshot.child("answers").child("false").getValue();
                    float truePercent = (trueVotes*100) / (float)answered;
                    float falsePercent = (falseVotes*100) / (float)answered;

                    tvTitle.setText(title);
                    if (!finished) {
                        tvStatus.setText("Poll is still active");
                    }
                    tvResponses.setText(answered + " responses");

                    ArrayList<Entry> barEntries = new ArrayList<>();
                    ArrayList<String> xData = new ArrayList<>();

                    barEntries.add(new Entry(truePercent, 0));
                    xData.add("True");
                    barEntries.add(new Entry(falsePercent, 1));
                    xData.add("False");

                    PieDataSet pieDataSet = new PieDataSet(barEntries, "Percent");
                    pieDataSet.setSliceSpace(6.f);

                    PieData pieData = new PieData(xData, pieDataSet);
                    pieData.setValueTextSize(16.f);
                    pieData.setValueFormatter(new PercentFormatter());

                    ArrayList<Integer> colorsList = new ArrayList<Integer> ();
                    colorsList.add(Color.rgb(192,255,140));
                    colorsList.add(Color.rgb(255,208,140));

                    pieDataSet.setColors(colorsList);
                    pcResult.setData(pieData);

                    pcResult.setDescription("");
                    pcResult.getLegend().setEnabled(false);

                    pcResult.setClickable(false);
                    pcResult.setUsePercentValues(true);
                    pcResult.setDrawSliceText(true);
                    pcResult.setHoleRadius(10.f);
                    pcResult.setFocusable(false);
                    pcResult.setFocusableInTouchMode(false);
                    pcResult.setRotationEnabled(true);
                    //pcResult.setTouchEnabled(false);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_lecturer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_create_poll) {
            Intent lecturerIntent = new Intent(this, NewPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
