Mobile response is android application that allowes users to create simple polls, and for other users to answer those polls.

For instance: 
Q: What is the capital of France?
a) Paris
b) London
c) 300
d) France has no capital city
e) Robert De Niro

Each user that uses this application has to be signed in, eather by google or by email and password registration.
You can use this application as a "Lecturer" or "The Audience".

Lecturer is the user who creates polls. There are several different poll types:

Multiple choice, One answer
Multiple choice, multiple answer
True-False Poll
Textual question, textual answer
Textual question, numerical answer

After creating a new poll, lecturer is given poll identifier which he then gives to his audience to answer the poll.
Each poll has unique identifier and each poll can be checked by the audience from its id.
This application is coded in android studio using google firebase for database storage and authentication.