package zavrad.fer.hr.mobileresponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Josip Kasap on 4/30/2017.
 */

public class Utility {
    public static final Map<Integer, Long> UNLOCK_MAP = new HashMap<>();
    public static final Map<Integer, Long> LOCK_MAP = new HashMap<>();

    static {
        UNLOCK_MAP.put(0, 0L);
        UNLOCK_MAP.put(1, 120000L);
        UNLOCK_MAP.put(2, 300000L);
        UNLOCK_MAP.put(3, 900000L);
        UNLOCK_MAP.put(4, 1800000L);
        UNLOCK_MAP.put(5, 3600000L);

        LOCK_MAP.put(0, 600000L);
        LOCK_MAP.put(1, 900000L);
        LOCK_MAP.put(2, 1800000L);
        LOCK_MAP.put(3, 3600000L);
        LOCK_MAP.put(4, 7200000L);
        LOCK_MAP.put(5, 21600000L);
        LOCK_MAP.put(6, 86400000L);
        LOCK_MAP.put(7, 172800000L);
        LOCK_MAP.put(8, 604800000L);
    }
}
