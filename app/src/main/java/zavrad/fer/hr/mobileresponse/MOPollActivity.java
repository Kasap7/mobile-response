package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MOPollActivity extends AppCompatActivity {

    private EditText etMOPollTitle;
    private ImageButton btnNewOption;
    private ImageButton btnRemoveOption;
    private Button btnBack;
    private Button btnCreatePoll;
    private TextView tvC;
    private TextView tvD;
    private TextView tvE;
    private EditText etA;
    private EditText etB;
    private EditText etC;
    private EditText etD;
    private EditText etE;

    private int selected = 2;
    private long poll_counter = -1;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabasePollCounter;

    private boolean liveView;
    private boolean privateView;
    private boolean multipleAnswers;
    private int unlockedPosition = 0;
    private int lockedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mopoll);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(MOPollActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Polls");
        mDatabasePollCounter = FirebaseDatabase.getInstance().getReference().child("poll_counter");

        mDatabasePollCounter.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                poll_counter = (Long)dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        etMOPollTitle = (EditText) findViewById(R.id.etMOPollTitle);
        etA = (EditText) findViewById(R.id.etA);
        etB = (EditText) findViewById(R.id.etB);
        etC = (EditText) findViewById(R.id.etC);
        etD = (EditText) findViewById(R.id.etD);
        etE = (EditText) findViewById(R.id.etE);
        tvC = (TextView) findViewById(R.id.tvC);
        tvD = (TextView) findViewById(R.id.tvD);
        tvE = (TextView) findViewById(R.id.tvE);
        btnNewOption = (ImageButton) findViewById(R.id.btnNewOption);
        btnRemoveOption = (ImageButton) findViewById(R.id.btnRemoveOption);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnCreatePoll = (Button) findViewById(R.id.btnCreatePoll);

        etC.setVisibility(View.INVISIBLE);
        etD.setVisibility(View.INVISIBLE);
        etE.setVisibility(View.INVISIBLE);
        tvC.setVisibility(View.INVISIBLE);
        tvD.setVisibility(View.INVISIBLE);
        tvE.setVisibility(View.INVISIBLE);

        btnNewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selected == 5) return;
                selected++;
                if (selected == 3) {
                    etC.setVisibility(View.VISIBLE);
                    tvC.setVisibility(View.VISIBLE);
                } else if (selected == 4) {
                    etD.setVisibility(View.VISIBLE);
                    tvD.setVisibility(View.VISIBLE);
                } else if (selected == 5) {
                    etE.setVisibility(View.VISIBLE);
                    tvE.setVisibility(View.VISIBLE);
                }
            }
        });

        btnRemoveOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selected == 2) return;
                selected--;
                if (selected == 2) {
                    etC.setVisibility(View.INVISIBLE);
                    tvC.setVisibility(View.INVISIBLE);
                } else if (selected == 3) {
                    etD.setVisibility(View.INVISIBLE);
                    tvD.setVisibility(View.INVISIBLE);
                } else if (selected == 4) {
                    etE.setVisibility(View.INVISIBLE);
                    tvE.setVisibility(View.INVISIBLE);
                }
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            liveView = bundle.getBoolean("live_view");
            privateView = bundle.getBoolean("private_view");
            multipleAnswers = bundle.getBoolean("multiple_answers");
            unlockedPosition = bundle.getInt("unlocked_position");
            lockedPosition = bundle.getInt("locked_position");
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MOPollActivity.this, NewPollActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                intent.putExtra("live_view", liveView);
                intent.putExtra("private_view", privateView);
                intent.putExtra("multiple_answers", multipleAnswers);
                intent.putExtra("unlocked_position", unlockedPosition);
                intent.putExtra("locked_position", lockedPosition);
                intent.putExtra("poll_type_position", 0);

                startActivity(intent);
            }
        });

        btnCreatePoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = etMOPollTitle.getText().toString().trim();
                String a = etA.getText().toString().trim();
                String b = etB.getText().toString().trim();
                String c = etC.getText().toString().trim();
                String d = etD.getText().toString().trim();
                String e = etE.getText().toString().trim();

                if (TextUtils.isEmpty(title) || TextUtils.isEmpty(a) || TextUtils.isEmpty(b)) {
                    Toast.makeText(MOPollActivity.this, "All fields must be submitted!", Toast.LENGTH_LONG).show();
                    return;
                }

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("title", title);

                Map<String, Object> answers = new HashMap<>();

                Map<String, Object> aAnswer = new HashMap<>();
                aAnswer.put("text", a);
                aAnswer.put("votes", 0L);
                answers.put("A", aAnswer);

                Map<String, Object> bAnswer = new HashMap<>();
                bAnswer.put("text", b);
                bAnswer.put("votes", 0L);
                answers.put("B", bAnswer);

                if (selected >= 3) {
                    if (TextUtils.isEmpty(c)) {
                        Toast.makeText(MOPollActivity.this, "All fields must be submitted!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Map<String, Object> cAnswer = new HashMap<>();
                    cAnswer.put("text", c);
                    cAnswer.put("votes", 0L);
                    answers.put("C", cAnswer);
                }

                if (selected >= 4) {
                    if (TextUtils.isEmpty(d)) {
                        Toast.makeText(MOPollActivity.this, "All fields must be submitted!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Map<String, Object> dAnswer = new HashMap<>();
                    dAnswer.put("text", d);
                    dAnswer.put("votes", 0L);
                    answers.put("D", dAnswer);
                }

                if (selected >= 5) {
                    if (TextUtils.isEmpty(e)) {
                        Toast.makeText(MOPollActivity.this, "All fields must be submitted!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Map<String, Object> eAnswer = new HashMap<>();
                    eAnswer.put("text", e);
                    eAnswer.put("votes", 0L);
                    answers.put("E", eAnswer);
                }

                childUpdates.put("answers", answers);
                long timeNow = System.currentTimeMillis();
                long unlock = timeNow + Utility.UNLOCK_MAP.get(unlockedPosition);
                long lock = timeNow + Utility.LOCK_MAP.get(lockedPosition);

                childUpdates.put("unlock", unlock);
                childUpdates.put("lock", lock);
                childUpdates.put("live_results", liveView);
                childUpdates.put("private_results", privateView);
                childUpdates.put("multiple_vote", multipleAnswers);
                childUpdates.put("creator", mAuth.getCurrentUser().getUid());
                childUpdates.put("answered", 0L);

                final String pollName = "mo" + poll_counter;
                mDatabase.child(pollName).updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mDatabasePollCounter.setValue(poll_counter + 1L);

                        Intent mainIntent = new Intent(MOPollActivity.this, MainActivity.class);
                        mainIntent.putExtra("new_poll", pollName);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_audience, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_vote_poll) {
            Intent lecturerIntent = new Intent(this, SearchPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
