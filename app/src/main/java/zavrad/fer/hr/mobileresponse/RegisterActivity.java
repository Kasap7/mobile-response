package zavrad.fer.hr.mobileresponse;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class RegisterActivity extends AppCompatActivity {

    private EditText mRePasswordField;
    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mSignUp;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;
    private static final String TAG = "RegisterActivity_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mProgress = new ProgressDialog(this);

        mRePasswordField = (EditText) findViewById(R.id.etRegisterRePassword);
        mEmailField = (EditText) findViewById(R.id.etRegisterEmail);
        mPasswordField = (EditText) findViewById(R.id.etRegisterPassword);
        mSignUp = (Button) findViewById(R.id.btnSignUp);

        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegister();
            }
        });
    }

    private void startRegister() {
        final String rePassword = mRePasswordField.getText().toString().trim();
        final String email = mEmailField.getText().toString().trim();
        final String password = mPasswordField.getText().toString().trim();

        if (TextUtils.isEmpty(rePassword) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "All fields are required!", Toast.LENGTH_LONG).show();
            return;
        }

        if (!password.equals(rePassword)) {
            mRePasswordField.setText("");
            Toast.makeText(this, "Password doesn't match!", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.length() < 8) {
            mRePasswordField.setText("");
            mPasswordField.setText("");
            Toast.makeText(this, "Password is too short!", Toast.LENGTH_LONG).show();
            return;
        }

        mProgress.setMessage("Signing up ...");
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mProgress.dismiss();

                    Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainIntent);
                } else {
                    mProgress.dismiss();
                    Toast.makeText(RegisterActivity.this, "Singing up failed: " + task.getException(), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }
}
