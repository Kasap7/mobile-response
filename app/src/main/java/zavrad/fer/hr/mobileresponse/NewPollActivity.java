package zavrad.fer.hr.mobileresponse;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class NewPollActivity extends AppCompatActivity {

    private CheckBox cbLiveView;
    private CheckBox cbPrivateView;
    private CheckBox cbMultipleAnswers;
    private Spinner spUnlock;
    private Spinner spLock;
    private Spinner spPollType;
    private Button btnNext;
    private int unlockedPosition = 0;
    private int lockedPosition = 0;
    private int pollTypePosition = 0;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_poll);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    Intent loginIntent = new Intent(NewPollActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                } else {
                }
            }
        };

        cbLiveView = (CheckBox) findViewById(R.id.cbLiveView);
        cbPrivateView = (CheckBox) findViewById(R.id.cbPrivateView);
        cbMultipleAnswers = (CheckBox) findViewById(R.id.cbMultipleAnswers);
        spUnlock = (Spinner) findViewById(R.id.spUnlock);
        spLock = (Spinner) findViewById(R.id.spLock);
        spPollType = (Spinner) findViewById(R.id.spPollType);
        btnNext = (Button) findViewById(R.id.btnNext);

        ArrayAdapter<CharSequence> unlockAdapter = ArrayAdapter.createFromResource(this,
                R.array.unlock_time, android.R.layout.simple_spinner_item);
        unlockAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUnlock.setAdapter(unlockAdapter);

        ArrayAdapter<CharSequence> lockAdapter = ArrayAdapter.createFromResource(this,
                R.array.lock_time, android.R.layout.simple_spinner_item);
        lockAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLock.setAdapter(lockAdapter);

        ArrayAdapter<CharSequence> pollTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.poll_types, android.R.layout.simple_spinner_item);
        pollTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPollType.setAdapter(pollTypeAdapter);

        spUnlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                unlockedPosition = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        spLock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                lockedPosition = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        spPollType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pollTypePosition = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (unlockedPosition == 3 && lockedPosition <= 1) {
                    Toast.makeText(NewPollActivity.this, "Unlocked time must be less then locked time", Toast.LENGTH_LONG).show();
                    return;
                }
                if (unlockedPosition == 4 && lockedPosition <= 2) {
                    Toast.makeText(NewPollActivity.this, "Unlocked time must be less then locked time", Toast.LENGTH_LONG).show();
                    return;
                }
                if (unlockedPosition == 5 && lockedPosition <= 3) {
                    Toast.makeText(NewPollActivity.this, "Unlocked time must be less then locked time", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent audienceIntent = null;
                if (pollTypePosition == 0) {
                    audienceIntent = new Intent(NewPollActivity.this, MOPollActivity.class);
                } else if (pollTypePosition == 1) {
                    audienceIntent = new Intent(NewPollActivity.this, MMPollActivity.class);
                } else if (pollTypePosition == 2) {
                    audienceIntent = new Intent(NewPollActivity.this, TFPollActivity.class);
                } else if (pollTypePosition == 3) {
                    audienceIntent = new Intent(NewPollActivity.this, TTPollActivity.class);
                    audienceIntent.putExtra("number", false);
                } else if (pollTypePosition == 4) {
                    audienceIntent = new Intent(NewPollActivity.this, TTPollActivity.class);
                    audienceIntent.putExtra("number", true);
                }

                if (audienceIntent == null) {
                    Toast.makeText(NewPollActivity.this, "Unsuported operation: " + pollTypePosition, Toast.LENGTH_LONG).show();
                    return;
                }

                audienceIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                audienceIntent.putExtra("live_view", cbLiveView.isChecked());
                audienceIntent.putExtra("private_view", cbPrivateView.isChecked());
                audienceIntent.putExtra("multiple_answers", cbMultipleAnswers.isChecked());
                audienceIntent.putExtra("unlocked_position", unlockedPosition);
                audienceIntent.putExtra("locked_position", lockedPosition);

                startActivity(audienceIntent);
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();

            boolean liveView = bundle.getBoolean("live_view");
            boolean privateView = bundle.getBoolean("private_view");
            boolean multipleAnswers = bundle.getBoolean("multiple_answers");
            unlockedPosition = bundle.getInt("unlocked_position");
            lockedPosition = bundle.getInt("locked_position");
            pollTypePosition = bundle.getInt("poll_type_position");

            cbLiveView.setChecked(liveView);
            cbPrivateView.setChecked(privateView);
            cbMultipleAnswers.setChecked(multipleAnswers);
            spUnlock.setSelection(unlockedPosition);
            spLock.setSelection(lockedPosition);
            spPollType.setSelection(pollTypePosition);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_audience, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logut) {
            logout();
        }

        if (item.getItemId() == R.id.action_vote_poll) {
            Intent lecturerIntent = new Intent(this, SearchPollActivity.class);
            lecturerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(lecturerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        mAuth.signOut();
    }
}
